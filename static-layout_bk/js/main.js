$(document).ready(function() {
    $('.div-jquery h4').click(function(){
        $('.div-jquery h4').removeClass('menuShow');
        $(this).addClass('menuShow');
        var tmp=$(this).next();
        if(tmp.is(':hidden') === true){
            $('.div-jquery .sub-custom').slideUp();
            tmp.slideDown();
            // $('.div-jquery h4 i').removeClass('arrow');
        }else{
            tmp.slideUp();
            $(this).removeClass('menuShow');
        }
    });

    var navListItems = $('ul.setup-panel li a'),
    allWells = $('.setup-content');

    allWells.hide();

    navListItems.click(function(e)
    {
    e.preventDefault();
    var $target = $($(this).attr('href')),
        $item = $(this).closest('li');

    if (!$item.hasClass('disabled')) {
        navListItems.closest('li').removeClass('active');
        $item.addClass('active');
        allWells.hide();
        $target.show();
    }
    });

    $('ul.setup-panel li.active a').trigger('click');

    $('#activate-step-2').on('click', function(e) {
    $('ul.setup-panel li:eq(1)').removeClass('disabled');
    $('ul.setup-panel li a[href="#step-2"]').trigger('click');
    $(this).remove();
    }) 

    $('#sandbox-container .input-group.date').datepicker({
    }); 
});