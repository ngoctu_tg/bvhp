/**
 * Created by Tri-Satria on 11/3/2015.
 */
jQuery(function($){
    $(document).on('mouseover','.custom-parent-menu',function(){
        var target = $(this).attr('data-target');
        $(target).addClass('show');
    });
    $(document).on('mouseover','.sub-menu-custom',function(){
        $(this).addClass('show');

    });
    $(document).on('mouseout','.sub-menu-custom',function(){
        $(this).removeClass('show');

    });

});